module type DB = Caqti_lwt.CONNECTION
open Caqti_type

let table = "public.user";;

type kind = Username | Email;;

let add_user =
    let sql_string = Printf.sprintf "insert into %s (username, email, password) values ($1, $2, $3)" table in
    let query = 
        let open Caqti_request.Infix in
            ((tup3 string string string) ->. unit) sql_string in
    fun ~username ~email ~password (module Db : DB) ->
        let%lwt unit_or_error = Db.exec query (username, email, password) in 
        Caqti_lwt.or_fail unit_or_error;;

let check_user =
    let sql_string = Printf.sprintf "select id from %s where email = $1 and password = $2" table in
    let query = 
        let open Caqti_request.Infix in
            ((tup2 string string) ->? string) sql_string in
    fun ~email ~password (module Db : DB) ->
        let%lwt opt_or_error = Db.find_opt query (email, password) in
        Caqti_lwt.or_fail opt_or_error;;

let exists kind =
    let sql_str = Printf.sprintf "select bool_or(%s = $1) as value from %s" kind table in
    let query =
        let open Caqti_request.Infix in
        (string ->! bool) sql_str in 
    fun value (module Db : DB) ->
        let () = Dream.log "%s - %s" sql_str value in
        let%lwt value_or_error = Db.find query value in
        Caqti_lwt.or_fail value_or_error;;

let validate ~req ~kind value =
    let exists = exists kind @@ value in
    let exists = Dream.sql req exists in
    let%lwt () =
        let%lwt exists = exists in
        Lwt.return @@ Dream.log (if exists then "valid" else "invalid") in
    exists;;

let signup ~req ~username ~email ~password =
    let%lwt () = Dream.sql req (add_user ~username ~email ~password) in
    Lwt.return @@ None;;

let signin ~req ~email ~password = 
    let%lwt id = Dream.sql req (check_user ~email ~password) in
    Lwt.return id;;
