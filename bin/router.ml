open Batteries

let handle_htmx ~req ~title ?(js="") body =
    let open Dream_html in
    let body = 
        match Dream.header req "Hx-Request" with
        | None      -> to_string @@ Templates.page ~title ~js body
        | Some _    -> 
                let title = to_string @@ HTML.title [] title in
                let body = to_string body in
                title ^ body in
    Dream.respond @@ body;;

let http_error ?(status=`Bad_Request) message =
    let open Dream_html in
    respond ~status @@ HTML.p [] [ txt "%s" message ];;

let home = Dream.get "/" (fun req -> handle_htmx ~req ~title:"quati" Pages.home);;

let static = Dream.get "/static/**" 
    @@ Dream.static
        ~loader:(fun _root path _req ->
            match Static.read path with
            | None          -> Dream.empty `Not_Found
            | Some asset    -> Dream.respond asset)
        "";;

let signup = 
    Dream.scope "signup" [] 
    [ Dream.get "" (fun req -> 
        handle_htmx ~req ~title:"quati - signup" ~js:("passconf") @@ Pages.signup req)

    ; Dream.post "" (fun req ->
        let handle_lwt res =
            match%lwt res with
            | None          -> Dream.redirect req "/"
            | Some error    -> http_error error in
        match%lwt Dream.form req with
        | `Ok   [ "email"   , email
                ; "password", password
                ; "username", username
                ]
            -> let res = User.signup ~req ~username ~email ~password in
                handle_lwt res
        | _ -> http_error "INVALID FORM"
    )

    ; Dream.get "/validate" (fun req -> 
        let fields = [ "username" ; "email" ; "password" ; "passconf" ] in 
        let find_field field =
            let () = Dream.log "%s" field in
            match Dream.query req field with
            | Some value -> Some (field, value)
            | _ -> None in
        let handle_value kind value =
            try
                if%lwt User.validate ~req ~kind value
                then http_error "ELSE"
                else
                    let open Dream_html in
                    respond @@ txt "✅"
            with error ->
                let () = Dream.log "ERROR %s" (Printexc.to_string error) in
                http_error "" in
        match List.find_map_opt find_field fields with
        | Some (kind, value) -> handle_value kind value
        | None ->
        http_error "NONE"
    )
    ];;
    
let signin = 
    Dream.scope "signin" [] 
    [ Dream.get "" (fun req -> 
        handle_htmx ~req ~title:"quati - signin" @@ Pages.signin req)
    
       
    ; Dream.post "" (fun req ->
        let get_body id =
            match%lwt id with
            | Some _ -> Dream.redirect req "/" 
            | None   -> http_error ~status:`Not_Found "NOT FOUND" in
        let%lwt body = Dream.body req in
        match%lwt Dream.form req with
        | `Ok   [ "email"   , email
                ; "password", password
                ]
            -> let id = User.signin ~req ~email ~password in
                get_body id
        | _ -> http_error body);
    ];;

let boards = 
    Dream.scope "boards" [] 
    [ Dream.get "" (fun req -> 
        let%lwt boards = Boards.boards ~req in
        handle_htmx ~req ~title:"quati - all boards" @@ Pages.boards ~req boards)

     ; Dream.post "" (fun req ->
        let handle_lwt res =
            match%lwt res with
            | None          -> Dream.redirect req "/boards"
            | Some error    -> http_error error in
        let new_board ?(is_private = false) title = 
            let res = Boards.new_board ~req ~title ~is_private in
                handle_lwt res in
        match%lwt Dream.form req with
        | `Ok   [ "private", is_private_str
                ; "title"  , title
                ]
            ->  let is_private = "on" = is_private_str in
                new_board ~is_private title 
        | `Ok   [ "title", title ] -> new_board title
        | _ -> http_error "INVALID FORM") 

     ; Dream.delete "/:id" (fun req ->
        let board_id = Dream.param req "id" in
        let handle_lwt res =
            match%lwt res with
            | None          -> Dream.redirect req "/boards"
            | Some error    -> http_error error in
        let delete_board id =
            let res = Boards.remove_board ~req ~id in
                handle_lwt res in
            delete_board board_id)

     ; Dream.get "/:id" (fun req -> 
        let board_id = Dream.param req "id" in
        let%lwt board_opt = Boards.board ~req board_id in
        match board_opt with
        | Some board -> handle_htmx ~req ~title:"quati - board by id" @@ Pages.board board
        | None -> http_error ~status:`Not_Found "Board not found")

     ; Dream.patch "/:id" (fun req ->
        let id = Dream.param req "id" in
        let handle_lwt res =
            match%lwt res with
            | None          -> Dream.redirect req @@ "/boards/" ^ id
            | Some error    -> http_error error in
        let edit_board title = 
            let res = Boards.update_board ~req ~title ~id in
                handle_lwt res in
        match%lwt Dream.form req with
        | `Ok   [ "title"  , title ]
            -> edit_board  title 
        | _ -> http_error "INVALID FORM") 

     ; Dream.get "/:id/edit" (fun req ->
        let id = Dream.param req "id" in
        let%lwt board_opt = Boards.board ~req id in
        match board_opt with
        | Some (id, title, _) -> 
            handle_htmx ~req ~title:"edit board" (Pages.edit_board_form ~id ~title req)
        | None -> http_error ~status:`Not_Found "Board not found")
    ];; 

let routes =
    [ home
    ; static
    ; signup
    ; signin
    ; boards
    ];;