let () =
    let interface = "0.0.0.0" in
    let port = 8080 in
    let pg = "postgresql://postgres:postgres@localhost:5433/quati" in
    Dream.run ~interface ~port
    @@ Dream.logger
    @@ Dream.memory_sessions
    @@ Dream.sql_pool pg
    @@ Dream.router Router.routes