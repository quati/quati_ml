open Dream_html
open HTML
type method' = GET | POST | DELETE | PATCH ;;

let a ~url text =
    a   [ href "%s" url
        ; Hx.boost true
        ; Hx.target "main"
        ]
        [ txt "%s" text ];;

let page ~title:title' ~js body' =
    let htmx_version = "1.9.11" in
    let htmx_integrity = "sha384-0gxUXCCR8yv9FM2b+U3FDbsKthCI66oH5IA9fHppQq9DDMHuMauqq1ZHBpJxQ0J0" in
    let stylesheet = "style.css" in
    let htmx_url = Printf.sprintf "https://unpkg.com/htmx.org@%s" htmx_version in
    html [ lang "en" ]
        [ head []
            [ title [] title' 
            ; meta [ charset "UTF-8" ]
            ; meta [ http_equiv `x_ua_compatible ; content "ID=edge" ]
            ; meta [ name "viewport" ; content "width=device-width, initial-scale=1.0" ]
            ; link [ rel "stylesheet" ; href "static/css/%s" stylesheet ]
            ; script    [ src "%s" htmx_url 
                        ; integrity "%s" htmx_integrity
                        ; crossorigin `anonymous 
                        ]
                        ""
            ; script [ src "%s/dist/ext/head-support.js" htmx_url ] ""
            ; script [ src "/static/js/%s.js" js ; defer ] ""
            ]
        ; body [ Hx.ext "head-support" ]
            [ h1 [] [ a ~url:"/" "quati" ] 
            ; nav []
                [ a  ~url:"/signup" "signup"
                ; a  ~url:"/signin" "signin" 
                ]
            ]
        ; main [] [ body' ]
        ]

let delete_button ~endpoint ~params ?(swap = "innerHTML") button_txt =
    form 
        [ Hx.delete "%s" endpoint
        ; Hx.target "main"
        ; Hx.params params
        ; Hx.swap "%s" swap
        ; action "%s" endpoint
        ]
        [ button [ type_ "submit" ] [ txt "%s" button_txt ] ]

let form ?(method' = POST) ~endpoint ~inputs ?(target="main") ?(params="*") ?(swap = "innerHTML") ~req button_txt =
    let method' = 
        match method' with
        | GET -> Hx.get 
        | POST -> Hx.post 
        | DELETE -> Hx.post 
        | PATCH -> Hx.patch in
    form 
        [ method' "%s" endpoint
        ; Hx.target "%s" target
        ; Hx.params "%s" params
        ; Hx.swap "%s" swap
        ]
        @@ (txt ~raw:true "%s" @@ Dream.csrf_tag req) 
        :: inputs 
        @ [ button [type_ "submit"] [txt "%s" button_txt] ]

        let input 
        ~type'
        ?(required'=true) 
        ?(validate=None) 
        ?(min_length=0) 
        ?(max_length=32)
        ?(min_number=0)
        ?(max_number=2048)
        ?(value'="")
        name' =
        let default_tags =
            [ id    "%s" name'
            ; name  "%s" name'
            ; type_ "%s" type'
            ] in
        let value_tag = [ value "%s" value' ] in
        let validate_tags =
            let length_tags = 
                [ minlength min_length
                ; maxlength max_length
                ] in
            let number_tags =
                match type' with
                | "number" ->
                    [ min "%d" min_number
                    ; max "%d" max_number
                    ] 
                | _ -> [] in
            length_tags @ number_tags in
        let htmx_validate_tags =
            match validate with
            | None     -> []
            | Some url -> 
                    [ Hx.get "%s" url 
                    ; Hx.target ".%s-validator" name' 
                    ; Hx.validate
                    ] in
        let required_tag =
            if required'
            then [ required ]
            else [] in
        section [ class_ "input" ]
            [ label [ for_ "%s" name' ] [ txt "%s" name' ]
            ; p [ class_ "%s-validator validator" name' ] []
            ; input 
                @@ default_tags
                @ required_tag
                @ value_tag
                @ validate_tags 
                @ htmx_validate_tags 
            ]
    