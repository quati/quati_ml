module type DB = Caqti_lwt.CONNECTION
open Caqti_type

let table = "public.board";;

type board = 
    { id: string 
    ; title: string 
    ; is_private: bool
    };;

let add_board = 
    let query = 
      let open Caqti_request.Infix in 
      ((tup2 string bool) ->. unit)
      @@ "INSERT INTO " ^ table ^ " (title, private) values ($1, $2)" in
    fun ~title ~is_private (module Db : DB) ->
        let%lwt unit_or_error = Db.exec query (title, is_private) in 
        Caqti_lwt.or_fail unit_or_error;;

let show_boards = 
  let query = 
    let open Caqti_request.Infix in
            (unit ->* (tup3 string string bool))
            @@ "SELECT * FROM " ^ table in
    fun () (module Db : DB) ->
        let%lwt opt_or_error = Db.collect_list query () in
        Caqti_lwt.or_fail opt_or_error;;

let show_board =
  let query =
    let open Caqti_request.Infix in
    (string ->? (tup3 string string bool))
    @@ "SELECT id, title, private FROM " ^ table ^ " WHERE id = ?" in
  fun id (module Db : DB) ->
    let%lwt opt_or_error = Db.find_opt query id in
    Caqti_lwt.or_fail opt_or_error;;

let delete_board = 
  let query = 
    let open Caqti_request.Infix in
        (string ->. unit)
        @@ "DELETE FROM " ^ table ^ " WHERE id = $1" in
    fun ~id (module Db : DB) ->
      let%lwt unit_or_error = Db.exec query (id) in 
      Caqti_lwt.or_fail unit_or_error;;

let edit_board = 
  let query = 
    let open Caqti_request.Infix in 
    ((tup2 string string) ->. unit)
    @@ "UPDATE " ^ table ^ " SET title = $1 WHERE id = $2" in
  fun ~title ~id (module Db : DB) ->
      let%lwt unit_or_error = Db.exec query (title, id) in 
      Caqti_lwt.or_fail unit_or_error;;
  
let boards ~req =
  let%lwt boards = Dream.sql req @@ show_boards () in
  Lwt.return boards;;

let board ~req id = 
  let%lwt board = Dream.sql req @@ show_board id in
  Lwt.return board;;

let new_board ~req ~title ~is_private =
  let%lwt () = Dream.sql req (add_board ~title ~is_private) in
  Lwt.return None;;

let remove_board ~req ~id = 
  let%lwt () = Dream.sql req @@ delete_board ~id in
  Lwt.return None;;

let update_board ~req ~id ~title =
  let%lwt () = Dream.sql req (edit_board ~title ~id) in
  Lwt.return None;;
    
let funcao tupla = 
  let a, b = tupla in
  b ^ "a", a + 1;;