CREATE TABLE public.board (
    id          UUID    PRIMARY KEY DEFAULT     uuid_generate_v4(),
    title		TEXT,
    private 	BOOLEAN
);