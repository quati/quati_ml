document.querySelector("form")?.addEventListener("htmx:beforeRequest", e => {
    console.log(e)
    const passList = [ "password", "passconf" ];
    const { validity, name, value } = e.target;

    if (!validity?.valid && (!passList.includes(name) || !validatePasswords(passList))) {
        e.preventDefault();
    }
});

function validatePasswords(passList) {
    const [ password, passconf ] = passList.map(pass => document.querySelector("#" + pass)?.value);
    return password === passconf;
}

