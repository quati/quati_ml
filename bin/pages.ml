open Dream_html
open HTML

let home = main [] [];;

let signup req = 
    let endpoint = "/signup" in
    Templates.form 
        ~endpoint
        ~params:"not passconf"
        ~inputs:
            (let open Templates in
            let validate = Some "/signup/validate" in
            [ input ~type':"text"       ~validate   ~min_length:2 ~max_length:16    "username"
            ; input ~type':"email"      ~validate   "email"
            ; input ~type':"password"   ~validate   ~min_length:8 ~max_length:32    "password"
            ; input ~type':"password"   ~validate   ~min_length:8 ~max_length:32    "passconf"
            ])
        ~req
        "sign up";;

let signin req = 
    let endpoint = "/signin" in
    Templates.form 
        ~params:"*"
        ~endpoint
        ~inputs:(let open Templates in
            let validate = Some "/signin/validate" in
            [ input ~type':"email"      ~validate   "email"
            ; input ~type':"password"   ~validate   "password"
            ])
        ~req
        "sign in";;

let boards ~req board =
    let p_of_board (board_id, board_title, _) =
        let open Dream_html in
        let open HTML in
        let endpoint = "/boards/" ^ board_id in
        let delete_form = Templates.delete_button
        ~params:"*"
        ~endpoint
        "delete" in
    a [href "/boards/%s" board_id; Hx.boost true; Hx.target "main"] [txt "%s" board_title; delete_form] in
    
    let boards_html = List.map p_of_board board in
    let endpoint = "/boards" in
    let form = Templates.form 
        ~params:"*"
        ~endpoint
        ~inputs:
        (let open Templates in
        [ input ~type':"text" "title"
        ; input ~type':"checkbox" ~required':false "private"
        ])
        ~req
        "submit" in
    
    main [] (form :: boards_html)
    ;;

let board board =
    let p_of_board board = 
        let open HTML in
        let (id, title, _) = board in
        let edit_button = 
            button [ 
                Hx.get "/boards/%s/edit" id;  
                Hx.target ".title-container"; 
                Hx.swap "outerHTML" 
            ] [ txt "Edit" ] 
        in
        div [ class_ "title-container" ] [
            p [] [ txt "%s" title ]; 
            edit_button
        ] in
    let board = p_of_board board in
    board;;

let edit_board_form ~id ~title req =
    let inputs = [
        Templates.input ~type':"text" 
                        ~value': title
                        "title"
    ] in
    Templates.form 
            ~method': PATCH
            ~endpoint: ("/boards/" ^ id)
            ~inputs
            ~swap: "outerHTML"
            ~req
            "save"