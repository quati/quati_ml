build:
	nix build

build_static:
	nix build .#prd

dev:
	nix develop -c $$SHELL

run:
	dune exec quati

watch:
	dune build -w @run

codium:
	nix develop -c codium .